package com.example.shemajamebeli4

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), GameAdapter.CustomCallback {

    private val squares = arrayListOf<SquareModel>()
    private val adapter = GameAdapter(squares, this)
    private var player = false
    private var squaresleft = 0
    private var winner = false
    private var gameSize = 0

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        if (numberEditText.text.toString().isNotEmpty()) {
            startButton.setOnClickListener {
                val number = numberEditText.text.toString().toInt()
                if (number in 3..5) {
                    squares.clear()
                    gameRecyclerView.layoutManager = GridLayoutManager(this, number)
                    gameRecyclerView.adapter = adapter
                    squaresleft = number * number
                    gameSize = number
                    winner = false
                    player = false
                    (0 until number * number).forEach {
                        squares.add(SquareModel(it, ""))
                    }
                    adapter.notifyDataSetChanged()
                }
            }
        }
    }


    private fun checkWinner() {
        if (squaresleft == 0 && !winner) {
            Toast.makeText(this, "Draw!", Toast.LENGTH_SHORT).show()
            winner = true
        }
        if (!winner) checkHorizontal()
        if (!winner) checkVertical()
        if (!winner) checkLeftDiagonal()
        if (!winner) checkRightDiagonal()
    }

    private fun checkLeftDiagonal() {
        var checker = 0
        val first = squares[0].type
        for (idx in 0 until gameSize * gameSize step gameSize + 1) {
            if (squares[idx].type == first && squares[idx].type != "") checker++
            if (checker == gameSize) {
                Toast.makeText(this, "Winner player is $first!", Toast.LENGTH_SHORT).show()
                winner = true
                break
            }
        }
    }

    private fun checkRightDiagonal() {
        var checker = 0
        val first = squares[gameSize - 1].type
        for (idx in gameSize - 1 until (gameSize * gameSize) - (gameSize - 1) step gameSize - 1) {
            if (squares[idx].type == first && squares[idx].type != "") checker++
            if (checker == gameSize) {
                Toast.makeText(this, "Winner player is $first!", Toast.LENGTH_SHORT).show()
                winner = true
                break
            }
        }
    }

    private fun checkHorizontal() {
        for (idx in 1..gameSize) {
            var checker = 0
            for (cube in gameSize * (idx - 1) until (gameSize * idx)) {
                val first = squares[gameSize * (idx - 1)].type
                if (squares[cube].type == first && squares[cube].type != "") checker++
                if (checker == gameSize) {
                    Toast.makeText(this, "Winner player $first", Toast.LENGTH_SHORT).show()
                    winner = true
                    break
                }
            }
        }
    }

    private fun checkVertical() {
        for (idx in 0..gameSize) {
            var checker = 0
            for (cube in idx until gameSize * gameSize step gameSize) {
                val first = squares[idx].type
                if (squares[cube].type == first && squares[cube].type != "") checker++
                if (checker == gameSize) {
                    Toast.makeText(this, "Winner player is $first!", Toast.LENGTH_SHORT).show()
                    winner = true
                    break
                }
            }
        }
    }

    override fun setSquareType(position: Int) {
        if (winner) return Toast.makeText(
            this,
            "Game Over",
            Toast.LENGTH_SHORT
        ).show()
        if (squares[position].type == "") {
            if (player) {
                squares[position].type = "O"
                player = false
            } else {
                squares[position].type = "X"
                player = true
            }
            squaresleft--
            checkWinner()
            adapter.notifyItemChanged(position)
        } else {
            Toast.makeText(this, "You cant do this!", Toast.LENGTH_SHORT).show()
        }
    }
}