package com.example.shemajamebeli4

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.square_layout.view.*

class GameAdapter(
    private val squares: ArrayList<SquareModel>,
    private val callback: CustomCallback
) : RecyclerView.Adapter<GameAdapter.ViewHolder>() {


    interface CustomCallback {
        fun setSquareType(position: Int)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            val model = squares[adapterPosition]
            when (model.type) {
                "X" -> itemView.squareImageButton.setBackgroundResource(R.drawable.ic_close_black_24dp)
                "O" -> itemView.squareImageButton.setImageResource(R.drawable.ic_exposure_zero_black_24dp)
                else -> itemView.squareImageButton.setImageResource(0)
            }
            itemView.squareImageButton.setOnClickListener {
                callback.setSquareType(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.square_layout, parent, false)
        )
    }

    override fun getItemCount(): Int = squares.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }
}